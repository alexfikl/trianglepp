#ifndef __UTILS_H__
#define __UTILS_H__

#include <stdbool.h>
#include <math.h>

#include "debug.h"
#include "mesh.h"

#define UNUSED(x) (void)(x)
#define MIN(x, y) ((x) > (y) ? (y) : (x))
#define MAX(x, y) ((x) > (y) ? (x) : (y))
#define FUZZYCMP(x, y) (fabs((x) - (y)) <= 0.000000000001 * MIN(fabs(x), fabs(y)))
#define ISNULL(x) (fabs(x) <= 0.000000000001)


/**
 * @brief Safe malloc.
 *
 * Exists if there isn't enough memory.
 *
 * @param[in] n The amount of memory to allocate.
 *
 * @returns The allocated memory.
 */
void* xmalloc(size_t n);

/**
 * @brief Safe calloc.
 *
 * Exists if there isn't enough memory.
 *
 * @param[in] n The number of elements to allocate.
 * @param[in] size The size of one element.
 *
 * @returns The allocated memory.
 */
void* xcalloc(size_t n, size_t size);

/**
 * @brief Checks if the file given by @c filename exists.
 *
 * @param[in] filename The name of the file.
 *
 * @returns @c True or if the file exists, @c False otherwise.
 */
bool file_exists(const char* filename);

/**
 * @brief A version of @c fgets that skips comments.
 *
 * Comments are defined as lines where the first character that isn't
 * whitespace is `#`.
 *
 * This will read lines until it finds one that isn't a comment.
 *
 * @param[in,out] buffer The buffer to read into.
 * @param[in] size The maximum number of characters to read.
 * @param[in] fd The file from which to read.
 *
 * @returns The first non-commented line.
 */
char* fgets_skip_comments(char* buffer, int size, FILE* fd);

/**
 * @brief Add an edge to the edge list of a node.
 *
 * @param[in,out] A The node.
 * @param[in] edge The edge.
 */
void node_add_edge(node_t* A, edge_t* edge);

/**
 * @brief Add a cell to the cell list of a node.
 *
 * @param[in,out] A The node.
 * @param[in] cell The cell.
 */
void node_add_cell(node_t* A, cell_t* cell);

/**
 * @brief Get the common edge between A and B.
 *
 * This checks the list of edges of A to see if any of them contains B.
 *
 * @param[in] A The first node.
 * @param[in] B The second node.
 *
 * @returns The edge that contains the two nodes or NULL if there is non.
 */
edge_t* node_common_edge(const node_t* A, const node_t* B);

/**
 * @brief Add a cell to the edge.
 *
 * This will add the new cell to Tj or Tk if they are NULL, else it will do
 * nothing.
 *
 * @param[in,out] e The edge.
 * @param[in] c The cell to add.
 */
void edge_add_cell(edge_t* e, cell_t* c);

/**
 * @brief Compute the length of an edge.
 *
 * @param[in] edge The edge.
 *
 * @returns The length of the edge.
 */
double edge_length(const edge_t* edge);

/**
 * @brief Check if a node belongs to an edge.
 *
 * @param[in] e The edge.
 * @param[in] A The node.
 *
 * @returns True if the edge contains the node, False otherwise.
 */
bool edge_contains_node(const edge_t* e, const node_t* A);

/**
 * @brief Compute the area of a cell.
 *
 * @param[in] cell The cell.
 *
 * @returns The area of the cell.
 */
double cell_area(const cell_t* cell);

/**
 * @brief Get the cell from the mesh.
 *
 * Does boundary checking on the @c id.
 *
 * @param[in] mesh The mesh.
 * @param[in] id The id of the cell in the mesh.
 *
 * @returns The cell.
 */
cell_t* mesh_get_cell(const mesh_t* mesh, long id);

/**
 * @brief Get the global id of a neighbour cell from the neighbour list index.
 *
 * @param[in] cell A cell.
 * @param[in] id The index of the neighbour we want the global id of.
 *
 * @returns The id of the neighbour or -1 if it's NULL.
 */
long cell_neigh_global_id(const cell_t* cell, int id);

/**
 * @brief The global id of one of the two cells that belong to an edge.
 *
 * @param[in] edge The edge
 * @param[in] id The id of the cell inside the edge (0 or 1).
 *
 * @returns The global id of the cell or -1 if it's NULL.
 */
long edge_cell_global_id(const edge_t* edge, int id);

/**
 * @brief Print all the information about a node.
 *
 * @param[in] n The node.
 */
void node_print(const node_t* n);

/**
 * @brief Print all the information about an edge.
 *
 * @param[in] e The edge.
 */
void edge_print(const edge_t* e);

/**
 * @brief Print some information about an edge.
 *
 * Prints: the id, the region, the coordinates of the nodes.
 *
 * @param[in] e The edge.
 */
void edge_print_short(const edge_t* e);

/**
 * @brief Print all the information about a cell.
 *
 * @param[in] c The cell.
 */
void cell_print(const cell_t* c);

#endif // __UTILS_H__
