#include <stdio.h>
#include <stdlib.h>

#include "utils.h"
#include "mesh.h"

/**
 * @brief A node comparison function.
 */
typedef bool (*cmp_t)(node_t *A, node_t *B);

/**
 * @brief Compare the regions of two edges.
 *
 * @param[in] a The first edge.
 * @param[in] b The second edge.
 *
 * @returns True if the region of @c a is bigger than the region of @c b.
 */
static int regioncmp(const void* a, const void* b)
{
    edge_t** e1 = (edge_t**)a;
    edge_t** e2 = (edge_t**)b;

    return (*e1)->region > (*e2)->region;
}

/**
 * @brief Compare two nodes based on their x coordinate.
 *
 * @param[in] A First node.
 * @param[in] B Second node.
 *
 * @return True if the two nodes have the same x coordinate.
 */
static bool compare_x(node_t* A, node_t *B)
{
    return (ISNULL(A->x) && ISNULL(B->x) ? true : FUZZYCMP(A->x, B->x));
}

/**
 * @brief Compare two nodes based on their y coordinate.
 *
 * @param[in] A First node.
 * @param[in] B Second node.
 *
 * @return True if the two nodes have the same y coordinate.
 */
static bool compare_y(node_t* A, node_t *B)
{
    return (ISNULL(A->y) && ISNULL(B->y) ? true : FUZZYCMP(A->y, B->y));
}

/**
 * @brief Compare two edges based on the x or y coordinates.
 *
 * @param[in] edge1 An edge.
 * @param[in] edge2 Another edge.
 * @param[in] compare A comparison function that compares the x or y
 *                    coordinate of the nodes in the cell.
 *
 * @return True if the two edges have the same x (or y) coordinate for both
 *         nodes.
 */
static bool compare_edge_coordinates(edge_t* edge, edge_t* match, cmp_t compare)
{
    return (compare(edge->A, match->A) && compare(edge->B, match->B)) ||
           (compare(edge->A, match->B) && compare(edge->B, match->A));
}

/**
 * @brief Swap two edge pointers.
 *
 * @param[in,out] e1 The first edge.
 * @param[in,out] e2 The second edge.
 */
static void edge_swap(edge_t** e1, edge_t** e2)
{
    edge_t* tmp = *e2;
    *e2 = *e1;
    *e1 = tmp;
}

/**
 * @brief Get the node in a cell that is opposite to a given edge.
 *
 * @param[in] c The cell.
 * @param[in] e The edge.
 *
 * @returns The node that is opposite to the edge @c e of the cell @c c.
 */
static node_t* cell_opposite_node(cell_t* c, edge_t* e)
{
    if(e == c->AB) {
        return c->C;
    } else if (e == c->BC) {
        return c->A;
    } else if (e == c->CA) {
        return c->B;
    } else {
        die("Cell[%ld]: No matching edges.", c->id);
    }

    return NULL;
}

/**
 * @brief Get the two nodes that belong to a cell in counterclockwise order.
 *
 * Use this function when you want the nodes in the order they are given in
 * a cell, which is the counterclockwise order, and not the order in the
 * edge, which is A - northern node, B - southern node.
 *
 * @param[in] c The cell.
 * @param[in] e The edge.
 * @param[in,out] A The first node of the edge.
 * @param[in,out] B The second node of the edge.
 */
static void cell_edge_nodes(cell_t* c, edge_t* e, node_t* A, node_t* B)
{
    if(e == c->AB) {
        *A = *(c->A);
        *B = *(c->B);
    } else if (e == c->BC) {
        *A = *(c->B);
        *B = *(c->C);
    } else if (e == c->CA) {
        *A = *(c->C);
        *B = *(c->A);
    } else {
        log_warn("Edge[%ld] not in Cell[%ld].", e->id, c->id);
    }
}

/**
 * @brief See if the cell c is the western cell with respect to the edge e.
 *
 * @param[in] e The edge.
 * @param[in] c The cell.
 *
 * @returns True if c is the western cell, False if not.
 */
static bool edge_west_cell(edge_t* e, cell_t* c)
{
    node_t* A = e->A;
    node_t* B = e->B;
    node_t* P = cell_opposite_node(c, e);

    /* definitely to the west */
    if(P->x < MIN(A->x, B->x)) {
        return true;
    }

    /* definitely to the east */
    if(P->x > MAX(A->x, B->x)) {
        return false;
    }

    /* equation of AB is : y = m * x + x0 */
    double m = (A->y - B->y) / (A->x - B->x);   /* slope of AB */
    double x0 = A->y - m * A->x;                /* point */
    double y0 = m * P->x + x0;                  /* point on AB for x = P.x */

    /* if the slope is positive and P.y is bigger: we are above -> west  */
    /* if the slope is negative and P.y is smaller: we are below -> west */
    if((m >= 0 && y0 < P->y) || (m < 0 && y0 > P->y)) {
        return true;
    }

    return false;
}

/**
 * @brief Compute the normal to the edge e that is exterior to a cell c.
 *
 * @param[in] e The edge.
 * @param[in] c The cell.
 *
 * @returns The exterior normal vector.
 */
static vect_t edge_exterior_normal(edge_t* e, cell_t* c)
{
    node_t A = { .x = 0.0, .y = 0.0 };
    node_t B = { .x = 0.0, .y = 0.0 };
    cell_edge_nodes(c, e, &A, &B);

    // this is based on the fact that the nodes in a cell are ordered
    // counterclockwise. so we can rotate the tangent vector by 90
    // degrees clockwise to get the exterior normal.
    vect_t n = {
        .x = (A.y - B.y),
        .y = -(A.x - B.x)
    };

    return n;
}

/**
 * @brief Compute the number of extra attributes for each element type.
 *
 * @param[in,out] mesh The mesh
 */
static void compute_nattrs(mesh_t* mesh)
{
    /* cells: 3 edges + area */
    mesh->ncellattrs = 4;

    /* edges: 2 cells + length + normal x + normal y */
    mesh->nedgeattrs = 5;

    /* nodes: max(nedges + ncells) */
    mesh->nnodeattrs = 0;
    for(long i = 0; i < mesh->nnodes; ++i) {
        mesh->nnodeattrs = MAX(mesh->nnodeattrs,
                               mesh->node[i]->nedges + mesh->node[i]->ncells);
    }
}

/**
 * @brief Sort the edges in a mesh by the region they belong to.
 *
 * @param[in,out] mesh The mesh.
 */
static void sort_boundary_edges_by_region(mesh_t* mesh)
{
    long j = 0;

    /* find the first non-boundary edge */
    while(mesh->edge[j]->region != 0) {
        ++j;
    }

    /* bring all the boundary edges to the beginning of the edge list */
    for(long i = j + 1; i < mesh->nedges; ++i) {
        /* swap if we found an boundary edge */
        if(mesh->edge[i]->region != 0) {
            edge_swap(&(mesh->edge[i]), &(mesh->edge[j]));
            ++j;
        }
    }

    mesh->nouteredges = j;

    /* sort */
    qsort(mesh->edge, mesh->nouteredges, sizeof(edge_t*), regioncmp);

    /* fix all the indexes */
    for(long i = 0; i < mesh->nedges; ++i) {
        mesh->edge[i]->id = i;
    }
}

/**
 * @brief Connect cells to edges.
 *
 * This is done quite easily because the nodes already contain a list of
 * edges and cells they belong to, so we just have to tie them all together.
 *
 * @param[in,out] mesh The mesh.
 */
static void connect_cell_edge(mesh_t* mesh)
{
    cell_t* cell;

    for(long i = 0; i < mesh->ncells; ++i) {
        cell = mesh->cell[i];

        cell->AB = node_common_edge(cell->A, cell->B);
        cell->BC = node_common_edge(cell->B, cell->C);
        cell->CA = node_common_edge(cell->C, cell->A);

        edge_add_cell(cell->AB, cell);
        edge_add_cell(cell->BC, cell);
        edge_add_cell(cell->CA, cell);
    }
}

/**
 * @brief Find out which of the cells belonging to an edge is the western cell.
 *
 * This also computes the exterior normal to the western cell once it is
 * found. If the edge is on the boundary edge and it has no western cell,
 * the normal is compute as exterior to the domain.
 *
 * @param[in,out] mesh The mesh.
 */
static void sort_edge_cells(mesh_t* mesh)
{
    cell_t* tmp;
    edge_t* e;

    for(long i = 0; i < mesh->nedges; ++i) {
        e = mesh->edge[i];

        /* make sure Tj is the western cell unless it's a boundary edge */
        if(e->Tk && !edge_west_cell(e, e->Tj)) {
            tmp = e->Tj;
            e->Tj = e->Tk;
            e->Tk = tmp;
        }

        /* compute the normal as exterior to Tj */
        e->normal = edge_exterior_normal(e, e->Tj);
    }
}

/**
 * @brief Find the matching edge corresponding to a given edge.
 *
 * In the case of periodic boundary conditions, we assume that for each
 * edge, there is another edge in our mesh that matches its x or y coordinates
 * but can be found on another boundary.
 *
 * @param[in] mesh The mesh.
 * @param[in] edge The edge to match.
 *
 * @return The matching edge.
 */
static edge_t* find_edge_match(mesh_t* mesh, edge_t *edge)
{
    edge_t *match = NULL;

    for(long i = 0; i < mesh->nouteredges; ++i) {
        match = mesh->edge[i];
        if (i == edge->id || edge->region == match->region) {
            continue;
        }

        if (compare_edge_coordinates(edge, match, compare_x) ||
                compare_edge_coordinates(edge, match, compare_y)) {
            return match;
        }
    }

    return NULL;
}

/**
 * @brief Match each boundary edge to a ghost edge and a ghost cell.
 *
 * In the case of Neuman boundary conditions, the ghost edge is the edge
 * itself and the ghost cell is the cell that contains the boundary edge.
 *
 * In the case of periodic boundary conditions, we have to find the
 * edge on the "other side" of the mesh and the cell it belongs to.
 *
 * @param[in] mesh The mesh.
 * @param[in] periodic A flag that indicates the type of the boundary (0 or 1).
 */
static void match_ghost_edges(mesh_t* mesh, int periodic)
{
    edge_t *match;

    mesh->pconn = xmalloc(mesh->nouteredges * sizeof(periodic_conn_t));
    for(long i = 0; i < mesh->nouteredges; ++i) {
        if (periodic) {
            match = find_edge_match(mesh, mesh->edge[i]);
        } else {
            match = mesh->edge[i];
        }
        check(match, "Cannot find a match for edge %ld.", i);

        mesh->pconn[i].edgeid = i;
        mesh->pconn[i].matchingedgeid = match->id;
        mesh->pconn[i].matchingcellid = match->Tj->id;
    }
}

void mesh_extra(mesh_t* mesh, int periodic)
{
    check(mesh, "The mesh is not initialized.");

    log_info("Computing the number of attributes per element.");
    compute_nattrs(mesh);

    log_info("Connecting cells to edges.");
    connect_cell_edge(mesh);

    log_info("Computing the western / eastern cell for all edges.");
    sort_edge_cells(mesh);

    if(mesh->markers) {
        log_info("Sorting edges by region.");
        sort_boundary_edges_by_region(mesh);

        log_info("Matching boundary edges to ghost edges and cells.");
        match_ghost_edges(mesh, periodic);
    }
}
