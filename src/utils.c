#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include "utils.h"

static bool is_comment(char* string)
{
    if(!string) {
        return false;
    }

    char* s = string;

    /* skip leading spaces */
    while(*s) {
        if (*s != ' ' || *s != '\t') {
            break;
        }
        s++;
    }

    if(*s == COMMENT) {
        return true;
    }
    return false;
}

void* xmalloc(size_t n)
{
    void *p = malloc(n);
    check_mem(p);

    return p;
}

void* xcalloc(size_t n, size_t size)
{
    void *p = calloc(n, size);
    check_mem(p);

    return p;
}

bool file_exists(const char* filename)
{
    return access(filename, F_OK) != -1;
}

char* fgets_skip_comments(char* buffer, int size, FILE* fd)
{
    do {
        // empty
    } while(fgets(buffer, size, fd) && is_comment(buffer));

    return buffer;
}

void node_add_edge(node_t* A, edge_t* edge)
{
    check(A->nedges < MAXEDGESPERNODE, "Increase MAXEDGESPERNODE.");

    A->edge[A->nedges] = edge;
    A->nedges += 1;
}

void node_add_cell(node_t* A, cell_t* cell)
{
    check(A->ncells < MAXCELLSPERNODE, "Increase MAXCELLSPERNODE.");

    A->cell[A->ncells] = cell;
    A->ncells += 1;
}

edge_t* node_common_edge(const node_t* A, const node_t* B)
{
    for(int i = 0; i < A->nedges; ++i) {
        if(edge_contains_node(A->edge[i], B)) {
            return A->edge[i];
        }
    }
    log_warn("Nodes %ld and %ld have no edge in common.", A->id, B->id);

    return NULL;
}

void edge_add_cell(edge_t* e, cell_t* c)
{
    if(e->Tj == NULL) {
        e->Tj = c;
    } else if(e->Tk == NULL) {
        e->Tk = c;
    } else {
        log_warn("Edge %ld is already part of two cells.", e->id);
    }
}

double edge_length(const edge_t* edge)
{
    node_t A = *(edge->A);
    node_t B = *(edge->B);

    return sqrt(pow(A.x - B.x, 2) + pow(A.y - B.y, 2));
}

bool edge_contains_node(const edge_t* e, const node_t* A)
{
    return (e->A == A) || (e->B == A);
}

double cell_area(const cell_t* cell)
{
    node_t A = *(cell->A);
    node_t B = *(cell->B);
    node_t C = *(cell->C);

    return fabs((A.x - C.x) * (B.y - A.y) - (A.x - B.x) * (C.y - A.y)) / 2;
}

cell_t* mesh_get_cell(const mesh_t* mesh, long id)
{
    if(id < 0 || id > mesh->ncells) {
        return NULL;
    }

    return mesh->cell[id];
}

long cell_neigh_global_id(const cell_t* cell, int id)
{
    if(cell->neigh[id] == NULL) {
        return -1;
    }

    return cell->neigh[id]->id;
}

long edge_cell_global_id(const edge_t* edge, int id)
{
    switch(id) {
    case 0:
        return (edge->Tj ? edge->Tj->id : -1);
        break;
    case 1:
        return (edge->Tk ? edge->Tk->id : -1);
        break;
    }

    return -1;
}

void node_print(const node_t* n)
{
    printf("=== Node[%ld](%lg, %lg) ===\n", n->id, n->x, n->y);
    printf("Region %d\n", n->region);
    for(int i = 0; i < n->nedges; ++i) {
        printf("Edge[%ld] ", n->edge[i]->id);
    }
    printf("\n");
    for(int i = 0; i < n->ncells; ++i) {
        printf("Cell[%ld] ", n->cell[i]->id);
    }
    printf("\n");
}

void edge_print(const edge_t* e)
{
    printf("=== Edge[%ld] === \n", e->id);
    printf("Region %d\n", e->region);
    printf("Node[%ld](%lg, %lg)\n", e->A->id, e->A->x, e->A->y);
    printf("Node[%ld](%lg, %lg)\n",e->B->id, e->B->x, e->B->y);
    printf("Cell[%ld]\nCell[%ld]\n", edge_cell_global_id(e, 0),
           edge_cell_global_id(e, 1));
    printf("Length = %lg\n", e->length);
    printf("Normal(%lg, %lg)\n", e->normal.x, e->normal.y);
}

void edge_print_short(const edge_t* e)
{
    printf("[id=%ld][r=%d] [%g %g] [%g %g]\n", e->id, e->region,
           e->A->x, e->A->y, e->B->x, e->B->y);
}

void cell_print(const cell_t* c)
{
    printf("=== Cell[%ld] ===\n", c->id);
    printf("Node[%ld](%lg, %lg)\n", c->A->id, c->A->x, c->A->y);
    printf("Node[%ld](%lg, %lg)\n", c->B->id, c->B->x, c->B->y);
    printf("Node[%ld](%lg, %lg)\n", c->C->id, c->C->x, c->C->y);
    printf("Edge[%ld] Edge[%ld] Edge[%ld]\n", c->AB->id, c->BC->id, c->CA->id);
    printf("Neighbours: %ld %ld %ld\n", cell_neigh_global_id(c, 0),
           cell_neigh_global_id(c, 1),
           cell_neigh_global_id(c, 2));
}
