#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "mesh.h"
#include "utils.h"

/*
 * TODO:
 *  - node: attrs: should we fill with -1?
 *  - node: attrs: read + write attributes provided by triangle?
 *  - node: edges + cells: better than allocating MAX*PERNODE?
 *  - all: preserve index from triangle? starts with 1 or 0. doesn't really
 *  matter because we write our own indexes.
 */

static void mesh_read_nodes(mesh_t* mesh, const char* filename)
{
    FILE *fd;
    fd = fopen(filename, "r");
    check(fd, "Unable to open file \"%s\" for reading.", filename);

    char buffer[MAXLINESIZE];
    long i = -1;
    int ret = 0;

    double x = 0.0;
    double y = 0.0;
    int region = -1;

    /* <# of nodes = int> <dimension = 2> <# of attributes> <# boundary markers = 0, 1> */
    fgets_skip_comments(buffer, MAXLINESIZE, fd);
    sscanf(buffer, "%ld %d %*d %d", &(mesh->nnodes),
           &(mesh->dim),
//                                     &(mesh->nnodedata),
           &(mesh->markers));

    /* allocate memory for all the nodes */
    mesh->node = xmalloc(mesh->nnodes * sizeof(node_t));

    /* read the nodes */
    for(i = 0; i < mesh->nnodes && !feof(fd); ++i) {
        fgets_skip_comments(buffer, MAXLINESIZE, fd);

        ret = sscanf(buffer, "%*d %lg %lg %d", &x, &y, &region);

        /* should always read 2 values. markers are optional. */
        check(ret == (2 + mesh->markers), "Invalid line %ld.", i);

        mesh->node[i] = xmalloc(sizeof(node_t));
        mesh->node[i]->id = i;
        mesh->node[i]->x = x;
        mesh->node[i]->y = y;
        mesh->node[i]->nedges = 0;
        mesh->node[i]->ncells = 0;
        mesh->node[i]->edge = xmalloc(MAXEDGESPERNODE * sizeof(edge_t*));
        mesh->node[i]->cell = xmalloc(MAXCELLSPERNODE * sizeof(cell_t*));
        mesh->node[i]->region = region;
    }

    check(i == mesh->nnodes, "Invalid file (must specify all nodes).");
    fclose(fd);
}

static void node_write_attrs(FILE *fd, node_t* node, int nattrs)
{
    int i = 0;
    int j = 0;

    /* write all the edges that contain this node */
    fprintf(fd, " %ld ", node->nedges);
    while(i < node->nedges) {
        fprintf(fd, "%ld ", node->edge[i]->id);
        ++i;
    }

    /* write all the cells that contain this node */
    fprintf(fd, "%ld ", node->ncells);
    while(j < node->ncells) {
        fprintf(fd, "%ld ", node->cell[j]->id);
        ++j;
    }

    /* fill the rest with -1s */
    i += j;
    while(i < nattrs) {
        fprintf(fd, "-1 ");
        ++i;
    }
}

static void mesh_write_nodes(const mesh_t* mesh, const char* filename)
{
    FILE* fd;
    fd = fopen(filename, "w");
    check(fd, "Unable to open file \"%s\" for writing.", filename);

    node_t* node = NULL;

    /* add two to attrs because we're also writing nnodes and nedges */
    fprintf(fd, "%ld %d ", mesh->nnodes, mesh->dim);
    fprintf(fd, "%d ", mesh->nnodeattrs + 2);
    fprintf(fd, "%d\n", mesh->markers);

    /* write the nodes and all their data */
    for(long i = 0; i < mesh->nnodes; ++i) {
        node = mesh->node[i];

        fprintf(fd, "%ld %lg %lg", node->id, node->x, node->y);
        node_write_attrs(fd, node, mesh->nnodeattrs);
        if(mesh->markers) {
            fprintf(fd, "%d\n", node->region);
        } else {
            fprintf(fd, "\n");
        }
    }

    fprintf(fd, "\n# file written with triangle++\n");
    fclose(fd);
}

static void mesh_read_edges(mesh_t* mesh, const char* filename)
{
    FILE* fd;
    fd = fopen(filename, "r");
    check(fd,  "Unable to open file \"%s\" for reading.", filename);

    char buffer[MAXLINESIZE];
    long i = -1;
    int ret = 0;

    long idA = -1;
    long idB = -1;
    int region = -1;

    /* <# of edges = int> <# boundary markers = 0, 1> */
    fgets_skip_comments(buffer, MAXLINESIZE, fd);
    sscanf(buffer, "%ld %*d", &(mesh->nedges));

    /* allocate memory for all the edges */
    mesh->edge = xmalloc(mesh->nedges * sizeof(edge_t));

    /* read the edges */
    for(i = 0; i < mesh->nedges && !feof(fd); ++i) {
        fgets_skip_comments(buffer, MAXLINESIZE, fd);

        ret = sscanf(buffer, "%*d %ld %ld %d", &idA, &idB, &region);
        check(ret == (2 + mesh->markers), "Invalid line %ld in edge file", i);

        /* make sure A is the northern node */
        mesh->edge[i] = xmalloc(sizeof(edge_t));
        if(mesh->node[idA]->y >= mesh->node[idB]->y) {
            mesh->edge[i]->A = mesh->node[idA];
            mesh->edge[i]->B = mesh->node[idB];
        } else {
            mesh->edge[i]->A = mesh->node[idB];
            mesh->edge[i]->B = mesh->node[idA];
        }

        mesh->edge[i]->id = i;
        mesh->edge[i]->Tj = NULL;
        mesh->edge[i]->Tk = NULL;
        mesh->edge[i]->length = edge_length(mesh->edge[i]);
        mesh->edge[i]->region = region;

        node_add_edge(mesh->edge[i]->A, mesh->edge[i]);
        node_add_edge(mesh->edge[i]->B, mesh->edge[i]);
    }

    check(i == mesh->nedges, "Invalid file (must specify all edges).");
    fclose(fd);
}

static void mesh_write_edges(const mesh_t* mesh, const char* filename)
{
    FILE* fd;
    fd = fopen(filename, "w");
    check(fd, "Unable to open file \"%s\" for writing.", filename);

    edge_t* edge = NULL;

    fprintf(fd, "%ld %d %d\n", mesh->nedges,
            mesh->nedgeattrs,
            mesh->markers);

    /* write all the edges */
    for(long i = 0; i < mesh->nedges; ++i) {
        edge = mesh->edge[i];

        fprintf(fd, "%ld ", edge->id);
        fprintf(fd, "%ld %ld ", edge->A->id, edge->B->id);
        fprintf(fd, "%ld %ld ", edge_cell_global_id(edge, 0),
                edge_cell_global_id(edge, 1));
        fprintf(fd, "%lg ", edge->length);
        fprintf(fd, "%lg %lg", edge->normal.x, edge->normal.y);
        if(mesh->markers) {
            fprintf(fd, " %d\n", edge->region);
        } else {
            fprintf(fd, "\n");
        }
    }

    fprintf(fd, "\n# file written with triangle++\n");
    fclose(fd);
}

static void mesh_read_cells(mesh_t* mesh, const char* filename)
{
    FILE *fd;
    fd = fopen(filename, "r");
    check(fd,  "Unable to open file \"%s\" for reading.", filename);

    char buffer[MAXLINESIZE];
    long i = -1;
    int ret = 0;

    long idA = -1;
    long idB = -1;
    long idC = -1;

    /* <# of cells> <# of nodes per cell> <# of attributes> */
    fgets_skip_comments(buffer, MAXLINESIZE, fd);
    sscanf(buffer, "%ld %*d %*d", &(mesh->ncells));

    /* allocate the memory */
    mesh->cell = xmalloc(mesh->ncells * sizeof(cell_t));

    for(i = 0; i < mesh->ncells && !feof(fd); ++i) {
        fgets_skip_comments(buffer, MAXLINESIZE, fd);

        ret = sscanf(buffer, "%*d %ld %ld %ld", &idA, &idB, &idC);
        check(ret == 3, "Invalid line %ld in ele file.", i);

        mesh->cell[i] = xmalloc(sizeof(cell_t));
        mesh->cell[i]->id = i;
        mesh->cell[i]->A = mesh->node[idA];
        mesh->cell[i]->B = mesh->node[idB];
        mesh->cell[i]->C = mesh->node[idC];
        mesh->cell[i]->AB = NULL;
        mesh->cell[i]->BC = NULL;
        mesh->cell[i]->CA = NULL;
        mesh->cell[i]->area = cell_area(mesh->cell[i]);

        node_add_cell(mesh->cell[i]->A, mesh->cell[i]);
        node_add_cell(mesh->cell[i]->B, mesh->cell[i]);
        node_add_cell(mesh->cell[i]->C, mesh->cell[i]);
    }

    check(i == mesh->ncells, "Invalid file (must specify all cells).");
    fclose(fd);
}

static void mesh_write_cells(const mesh_t* mesh, const char* filename)
{
    FILE* fd;
    fd = fopen(filename, "w");
    check(fd, "Unable to open file \"%s\" for writing.", filename);

    cell_t* cell = NULL;

    /* <# of cells> <# of nodes per cell> <# of attributes> */
    fprintf(fd, "%ld 3 %d\n", mesh->ncells, mesh->ncellattrs);

    for(long i = 0; i < mesh->ncells; ++i) {
        cell = mesh->cell[i];

        fprintf(fd, "%ld ", cell->id);
        fprintf(fd, "%ld %ld %ld ", cell->A->id, cell->B->id, cell->C->id);
        fprintf(fd, "%ld %ld %ld ", cell->BC->id, cell->CA->id, cell->AB->id);
        fprintf(fd, "%lg\n", cell->area);
    }

    fprintf(fd, "\n# file written with triangle++\n");
    fclose(fd);
}

static void mesh_read_neighs(mesh_t* mesh, const char* filename)
{
    FILE* fd;
    fd = fopen(filename, "r");
    check(fd, "Unable to open file \"%s\" for reading.", filename);

    char buffer[MAXLINESIZE];
    long i = -1;
    int ret = 0;

    long idT1 = -1;
    long idT2 = -1;
    long idT3 = -1;

    /* <# of cells> <# of neighbours per cell> */
    fgets_skip_comments(buffer, MAXLINESIZE, fd);
    sscanf(buffer, "%*d %d %*d", &(mesh->ncellneigh));

    for(i = 0; i < mesh->ncells; ++i) {
        fgets_skip_comments(buffer, MAXLINESIZE, fd);

        ret = sscanf(buffer, "%*d %ld %ld %ld", &idT1, &idT2, &idT3);
        check(ret == 3, "Invalid line %ld in neigh file.", i);

        mesh->cell[i]->neigh = xmalloc(mesh->ncellneigh * sizeof(cell_t*));
        mesh->cell[i]->neigh[0] = mesh_get_cell(mesh, idT1);
        mesh->cell[i]->neigh[1] = mesh_get_cell(mesh, idT2);
        mesh->cell[i]->neigh[2] = mesh_get_cell(mesh, idT3);
    }

    check(i == mesh->ncells, "Invalid file (must specify all cells neighbours).");
    fclose(fd);
}

static void mesh_write_neighs(const mesh_t* mesh, const char* filename)
{
    FILE* fd;
    fd = fopen(filename, "w");
    check(fd, "Unable to open file \"%s\" for writing.", filename);

    cell_t* cell = NULL;

    /* <# of cells> <# number of neighbours per cell> */
    fprintf(fd, "%ld %d\n", mesh->ncells, mesh->ncellneigh);

    for(long i = 0; i < mesh->ncells; ++i) {
        cell = mesh->cell[i];

        fprintf(fd, "%ld", cell->id);
        for(int j = 0; j < mesh->ncellneigh; ++j) {
            fprintf(fd, " %ld", cell_neigh_global_id(cell, j));
        }
        fprintf(fd, "\n");
    }

    fprintf(fd, "\n# file written with triangle++\n");
    fclose(fd);
}

static void mesh_write_conn(const mesh_t* mesh, const char* filename)
{
    FILE* fd;
    fd = fopen(filename, "w");
    check(fd, "Unable to open file \"%s\" for writing.", filename);

    /* <# of boundary edges> */
    fprintf(fd, "%ld\n", mesh->nouteredges);

    for(long i = 0; i < mesh->nouteredges; ++i) {
        fprintf(fd, "%ld %ld %ld\n", mesh->pconn[i].edgeid,
                mesh->pconn[i].matchingedgeid,
                mesh->pconn[i].matchingcellid);
    }

    fprintf(fd, "\n# file written with triangle++\n");
    fclose(fd);
}

mesh_t* mesh_init()
{
    return xcalloc(1, sizeof(mesh_t));
}

int mesh_free(mesh_t* mesh) {
    if(mesh == NULL) {
        return 0;
    }

    /* free the nodes */
    if(mesh->node) {
        debug("Freeing nodes.");
        for(long i = 0; i < mesh->nnodes; ++i) {
            free(mesh->node[i]->edge);
            free(mesh->node[i]->cell);
            free(mesh->node[i]);
        }

        free(mesh->node);
    }

    /* free the edges */
    if(mesh->edge) {
        debug("Freeing edges.");
        for(long i = 0; i < mesh->nedges; ++i) {
            free(mesh->edge[i]);
        }

        free(mesh->edge);
    }

    /* free the cells */
    if(mesh->cell) {
        debug("Freeing cells.");
        for(long i = 0; i < mesh->ncells; ++i) {
            free(mesh->cell[i]->neigh);
            free(mesh->cell[i]);
        }
        free(mesh->cell);
    }

    /* finally! */
    free(mesh);
    debug("Mesh freed.");

    return 0;
}

void mesh_read(mesh_t *mesh, const char *src)
{
    check(mesh, "The mesh is uninitialized.");

    /* length = src length + extension length + \0 */
    long length = strlen(src) + strlen(".neigh") + 1;
    char *tmpfile = xmalloc(length * sizeof(char));

    sprintf(tmpfile, "%s.node", src);
    mesh_read_nodes(mesh, tmpfile);

    sprintf(tmpfile, "%s.edge", src);
    mesh_read_edges(mesh, tmpfile);

    sprintf(tmpfile, "%s.ele", src);
    mesh_read_cells(mesh, tmpfile);

    sprintf(tmpfile, "%s.neigh", src);
    mesh_read_neighs(mesh, tmpfile);

    free(tmpfile);
}

void mesh_write(const mesh_t* mesh, const char* dst)
{
    check(mesh, "The mesh is uninitialized.");

    /* length = dst length + extension length + '\0' */
    long length = strlen(dst) + strlen(".neighpp") + 1;
    char* tmpfile = xmalloc(length * sizeof(char));

    snprintf(tmpfile, length, "%s.nodepp", dst);
    mesh_write_nodes(mesh, tmpfile);

    snprintf(tmpfile, length, "%s.edgepp", dst);
    mesh_write_edges(mesh, tmpfile);

    snprintf(tmpfile, length, "%s.elepp", dst);
    mesh_write_cells(mesh, tmpfile);

    snprintf(tmpfile, length, "%s.neighpp", dst);
    mesh_write_neighs(mesh, tmpfile);

    if (mesh->markers) {
        snprintf(tmpfile, length, "%s.connpp", dst);
        mesh_write_conn(mesh, tmpfile);
    }

    free(tmpfile);
}
