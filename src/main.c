#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mesh.h"

int main(int argc, char** argv)
{
    int filepos = 1;
    int periodic = 0;

    if(argc <= 1) {
        printf("usage: %s [--periodic] BASENAME\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    if (argc == 3 && strcmp(argv[1], "--periodic") == 0) {
        filepos = 2;
        periodic = 1;
    } else {
        filepos = 1;
    }

    mesh_t* mesh = mesh_init();

    mesh_read(mesh, argv[filepos]);
    mesh_extra(mesh, periodic);
    mesh_write(mesh, argv[filepos]);

    mesh_free(mesh);

    return EXIT_SUCCESS;
}
