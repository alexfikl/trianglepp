/*
 * Debug header for C stuff. Don't go anywhere without it.
 */

#ifndef __DEBUG_H__
#define __DEBUG_H__

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

#define clean_errno() (errno == 0 ? "None" : strerror(errno))

#ifdef NDEBUG
#define debug(M, ...)
#else
#define debug(M, ...) fprintf(stderr, "DEBUG %s:%s:%d: " M "\n", \
    __FILENAME__, __func__, __LINE__, ##__VA_ARGS__)
#endif

#define log_err(M, ...) fprintf(stderr, "[ERROR] (%s:%s:%d: errno: %s) " M "\n", \
    __FILENAME__, __func__, __LINE__, clean_errno(), ##__VA_ARGS__)

#define log_warn(M, ...) fprintf(stderr, "[WARN] (%s:%s:%d: errno: %s) " M "\n", \
    __FILENAME__, __func__, __LINE__, clean_errno(), ##__VA_ARGS__)

#define log_info(M, ...) fprintf(stderr, "[INFO] (%s:%s:%d) " M "\n", \
    __FILENAME__, __func__, __LINE__, ##__VA_ARGS__)

#define die(M, ...) do { \
    log_err(M, ##__VA_ARGS__); exit(EXIT_FAILURE); \
    } while(0);

#define check(A, M, ...) if(!(A)) { die(M, ##__VA_ARGS__) }

#define check_mem(A) check((A), "Out of memory.")

#endif
